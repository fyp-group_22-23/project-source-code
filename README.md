# Project Source Code (and any useful material)

## Demo App
https://gitlab.com/actka/demoApp/-/tags/Apr22

### screenshots
https://gitlab.com/actka/demoApp/-/tree/Apr22/screenshots


## ROS Laucnh Script 
https://gitlab.com/actka/actka-scripts


## Motor Controller Programs
https://github.com/kevinhochur/l298n_pwm_control.git


## Navigation Stack Library
https://github.com/kevinhochur/actka_navigation.git


## IPS solution
https://drive.google.com/drive/folders/13jv2up4iJ_ToAmOfJrltHFyzXOT6FSsA?usp=sharing
